package pl.edu.pwsztar.domain.chess;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class RookTest {

    private RulesOfGame rook = new RulesOfGame.Rook();

    @Tag("Rook")
    @ParameterizedTest
    @CsvSource({
            " 6,  0,  6,  20",
            "-1, 8, -21, 8",
            "-1,  5, -3,   5 ",
            " 5,  1,  5,  -1 ",
    })
    void checkCorrectMoveForRook(int xStart, int yStart, int xStop, int yStop) {
        assertTrue(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }

    @ParameterizedTest
    @CsvSource({
            "0,  1,  1,   -2",
            "10, 6, 9,  7"
    })
    void checkIncorrectMoveForRook(int xStart, int yStart, int xStop, int yStop) {
        assertFalse(rook.isCorrectMove(xStart, yStart, xStop, yStop));
    }
}
