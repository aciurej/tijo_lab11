package pl.edu.pwsztar.domain.chess;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

public interface RulesOfGame {

    boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd);

    @Component
    @Qualifier("Bishop")
    class Bishop implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }

            return Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart);
        }
    }

    @Component
    @Qualifier("Knight")
    class Knight implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }

            if(Math.abs(xStart-xEnd) == 2 && Math.abs(yStart - yEnd) == 1 || Math.abs(xStart-xEnd) == 1 && Math.abs(yStart - yEnd) == 2) {
                return true;
            }else{
                return false;
            }
        }
    }

    @Component
    @Qualifier("King")
    class King implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            if(Math.abs(xEnd - xStart) < 2 && Math.abs(yEnd - yStart) < 2){
                return true;
            }else{
                return false;
            }
        }
    }

    @Component
    @Qualifier("Queen")
    class Queen implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }

            if((Math.abs(xEnd - xStart) == Math.abs(yEnd - yStart)) || (xStart == xEnd && yStart!= yEnd) || (xStart != xEnd && yStart == yEnd)){
                return true;
            }else{
                return false;
            }

        }
    }

    @Component
    @Qualifier("Rook")
    class Rook implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            if((xStart == xEnd && yStart!= yEnd) || (xStart != xEnd && yStart == yEnd)){
                return true;
            }else{
                return false;
            }
        }
    }

    @Component
    @Qualifier("Pawn")
    class Pawn implements RulesOfGame {

        @Override
        public boolean isCorrectMove(int xStart, int yStart, int xEnd, int yEnd) {
            if(xStart == xEnd && yStart == yEnd) {
                return false;
            }
            if(yEnd-yStart == 1 && xStart==xEnd){
                return true;
            }else {
                return false;
            }

            }
        }
}
