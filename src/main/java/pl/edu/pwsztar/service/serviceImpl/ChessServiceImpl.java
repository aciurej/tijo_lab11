package pl.edu.pwsztar.service.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import pl.edu.pwsztar.domain.chess.RulesOfGame;
import pl.edu.pwsztar.domain.dto.FigureMoveDto;
import pl.edu.pwsztar.domain.enums.FigureType;
import pl.edu.pwsztar.service.ChessService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class ChessServiceImpl implements ChessService {

    private RulesOfGame bishop;
    private RulesOfGame knight;
    private RulesOfGame queen;
    private RulesOfGame pawn;
    private RulesOfGame king;
    private RulesOfGame rook;

    @Autowired
    public ChessServiceImpl(@Qualifier("Bishop") RulesOfGame bishop,
                            @Qualifier("Knight") RulesOfGame knight,
                            @Qualifier("King") RulesOfGame king,
                            @Qualifier("Queen") RulesOfGame queen,
                            @Qualifier("Rook") RulesOfGame rook,
                            @Qualifier("Pawn") RulesOfGame pawn) {
        this.bishop = bishop;
        this.knight = knight;
        this.king = king;
        this.queen = queen;
        this.rook = rook;
        this.pawn = pawn;
    }

    @Override
    public boolean isCorrectMove(FigureMoveDto figureMoveDto){
        return validateMove(figureMoveDto);
    }

    private boolean validateMove(FigureMoveDto figureMoveDto){
        String[] startPos = figureMoveDto.getStart().split("_");
        String[] desPos = figureMoveDto.getDestination().split("_");
        List<String> positions = new ArrayList<>(Arrays.asList("a","b","c","d","e","f","g","h"));
        int currentX = positions.indexOf(startPos[0]);
        int currentY = Integer.parseInt(startPos[1]);
        int newX = positions.indexOf(desPos[0]);
        int newY = Integer.parseInt(desPos[1]);

        if(figureMoveDto.getType() == FigureType.BISHOP){
            return bishop.isCorrectMove(currentX,currentY,newX,newY);
        }
        if(figureMoveDto.getType() == FigureType.PAWN){
            return pawn.isCorrectMove(currentX,currentY,newX,newY);
        }
        if(figureMoveDto.getType() == FigureType.QUEEN){
            return queen.isCorrectMove(currentX,currentY,newX,newY);
        }
        if(figureMoveDto.getType() == FigureType.KING){
            return king.isCorrectMove(currentX,currentY,newX,newY);
        }
        if(figureMoveDto.getType() == FigureType.ROOK){
            return rook.isCorrectMove(currentX,currentY,newX,newY);
        }
        if(figureMoveDto.getType() == FigureType.KNIGHT){
            return knight.isCorrectMove(currentX,currentY,newX,newY);
        }
        return false;
    }

}
